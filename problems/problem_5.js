
// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.
function problem_5(inventory,year){
    if(!inventory || typeof inventory===Object){
        return console.log("inventory not avalible");
    }
    let total_cars=[]
    for(let elements of inventory){
        if(elements.car_year>year){
            total_cars.push(elements.car_make)

        }

    }
    return (console.log(total_cars , `total_cars : ${total_cars.length}`))

}

module.exports={
    problem_5
}
