// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of:
//"Last car is a *car make goes here* *car model goes here*"

function Problem_2(inventory){
    if(!inventory || typeof inventory===Object){
        return console.log("inventory not avalible");
    }
    let last_car=inventory[inventory.length-1]
    for(let elements of inventory){
        if(elements.id==last_car.id){
            console.log(`Last car is a ${elements.car_make} ${elements.car_model}`)
        }
    }
}

module.exports={
    Problem_2
}
