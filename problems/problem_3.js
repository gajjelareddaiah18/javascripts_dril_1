// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.
  

function Problem_3(inventory){
  if(!inventory || typeof inventory===Object){
    return console.log("inventory not avalible");
}
    let modelNames=[]
    for(let elements of inventory){
        modelNames.push(elements.car_model)

    }
    for (let i = 0; i < modelNames.length - 1; i++) {
        for (let j = i + 1; j < modelNames.length; j++) {
          if (modelNames[i].toLowerCase() > modelNames[j]. toLowerCase() ) {
            const temp = modelNames[i];
            modelNames[i] = modelNames[j];
            modelNames[j] = temp;
          }
        }
      }
    
      return modelNames
    }




module.exports={
    Problem_3

}