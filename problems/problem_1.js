


// ==== Problem #1 ====
// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of:
//"Car 33 is a *car year goes here* *car make goes here* *car model goes here*"

function problem_1(inventory,num){
    if(!inventory || typeof inventory===Object){
        return console.log("inventory not avalible");
    }
    for(let elements of inventory){
        if(elements.id==num){
            console.log(`Car ${num} is a ${elements.car_year} ${elements.car_make} ${elements.car_model}`)
        }
    }
}

module.exports={
    problem_1

}

