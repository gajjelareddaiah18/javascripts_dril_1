// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.

function problem_4(inventory){
    if(!inventory || typeof inventory===Object){
        return console.log("inventory not avalible");
    }
    let all_cars_yeras=[]
    for(let elements of inventory){
        all_cars_yeras.push(elements.car_year)
    }
    return all_cars_yeras


}
module.exports={
    problem_4
}